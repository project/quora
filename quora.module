<?php
/**
 * @file
 * Contains block, theme, hooks for Quora module.
 */

/**
 * Implements hook_help().
 */
function quora_help($path, $arg) {
  switch ($path) {
    case 'admin/help#quora':
      $output = file_get_contents(drupal_get_path('module', 'quora') . '/README.txt');
      return $output;
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function quora_ctools_plugin_directory($module, $plugin) {
  if (($module == 'ctools') && ($plugin == 'content_types')) {
    return 'plugins/content_types';
  }
}

/**
 * Implements hook_menu().
 */
function quora_menu() {

  // Menu item for module configurations.
  $items['admin/config/quora'] = array(
    'title' => 'Quora Configuration',
    'description' => 'Configure quora for content types.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('quora_config_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'quora.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function quora_theme($existing, $type, $theme, $path) {
  return array(
    'quora_que_item' => array(
      'template' => 'quora-que-item',
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function quora_block_info() {
  $blocks['quora_block'] = array(
    'info' => 'Related Quora Questions',
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function quora_block_configure($delta = '') {
  $form = array();
  if ($delta == 'quora_block') {
    _quora_settings_form('block', $form);
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function quora_block_save($delta = '', $edit = array()) {
  if ($delta == 'quora_block') {
    variable_set('quora_no_questions', $edit['quora_no_questions']);
    variable_set('quora_description', $edit['quora_description']);
    variable_set('quora_description_size', $edit['quora_description_size']);
    variable_set('quora_search_sensitivity', $edit['quora_search_sensitivity']);
    variable_set('quora_include', $edit['quora_include']);
    variable_set('quora_exclude', $edit['quora_exclude']);
  }
}

/**
 * Implements hook_block_view().
 */
function quora_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'quora_block':
      if (user_access('access content')) {
        $block['subject'] = t('Quora');
        $block['content'] = _quora_content();
      }
      break;
  }
  return $block;
}

/**
 * Returns content for block.
 */
function _quora_content() {
  // Getting available contexts from menu.
  $context = menu_get_object();
  if (!($context && isset($context->nid))) {
    return NULL;
  }
  return _quora_build_content('block', $context);
}

/**
 * Builds Settings Form.
 */
function _quora_settings_form($callby, &$form, $form_state = NULL, $conf = NULL) {
  $form['quora_display_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Options'),
  );
  $form['quora_display_options']['quora_no_questions'] = array(
    '#type' => 'select',
    '#title' => t('Number of related questions to be shown'),
    '#options' => array(
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
      6 => '6',
      7 => '7',
      8 => '8',
    ),
    '#default_value' => _quora_default($callby, 'no_questions', $conf),
  );
  $form['quora_display_options']['quora_description'] = array(
    '#type' => 'select',
    '#title' => t('Description with questions'),
    '#options' => array(
      'enable' => t('Enable'),
      'disable' => t('Disable'),
    ),
    '#default_value' => _quora_default($callby, 'description', $conf),
  );
  $form['quora_display_options']['quora_description_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit Description text'),
    '#description' => t('Enter size, 0 for no limit'),
    '#element_validate' => array('element_validate_integer'),
    '#default_value' => _quora_default($callby, 'description_size', $conf),
    '#states' => array(
      'visible' => array(
        ':input[name="quora_description"]' => array('value' => 'enable'),
      ),
    ),
  );
  $form['quora_search_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Search Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['quora_search_options']['quora_search_sensitivity'] = array(
    '#type' => 'select',
    '#title' => t('Search Sensitivity'),
    '#options' => array(
      0 => t('Auto'),
      1 => t('3 Words'),
      2 => t('5 Words'),
      3 => t('7 Words'),
      4 => t('Maximum'),
    ),
    '#default_value' => _quora_default($callby, 'search_sensitivity', $conf),
  );
  $form['quora_search_options']['quora_include'] = array(
    '#type' => 'textfield',
    '#title' => t('Always include certain words'),
    '#description' => t('Use comma to separate multiple words. (Case Insensitive)'),
    '#default_value' => _quora_default($callby, 'include', $conf),
  );
  $form['quora_search_options']['quora_exclude'] = array(
    '#type' => 'textfield',
    '#title' => t('Always exclude certain words'),
    '#description' => t('Use comma to separate multiple words. (Case Insensitive)'),
    '#default_value' => _quora_default($callby, 'exclude', $conf),
  );
}

/**
 * Builds Quora Content form.
 */
function _quora_build_content($callby, $node, $conf = NULL) {
  $quora_tag_field = _quora_fetch_field($node);
  $quora_tag_string = _quora_preprocess_tag_terms($callby, $node, $quora_tag_field, $conf);
  _quora_filter_tag_terms($callby, $quora_tag_string, $conf);
  $results = _quora_search_web($quora_tag_string);
  if (!$results) {
    drupal_set_message(t('No results were found'), 'error', FALSE);
    return NULL;
  }
  return _quora_process_results($callby, $results, $conf);
}


/**
 * Returns field which will be used as tag field by quora.
 */
function _quora_fetch_field($node) {
  $mapped_field = variable_get('quora_' . $node->type . '_field', FALSE);
  if (isset($node->{$mapped_field}) && !empty($node->{$mapped_field})) {
    return $mapped_field;
  }
  elseif (isset($node->field_tags) && !empty($node->field_tags)) {
    return 'field_tags';
  }
  else {
    return 'title';
  }
}

/**
 * Returns array of preprocessed tags according to sensitivity.
 */
function _quora_preprocess_tag_terms($callby, $node, $quora_tag_field, $conf = NULL) {
  $data = entity_metadata_wrapper('node', $node)->{$quora_tag_field}->value();
  // Formation of Data string.
  switch (gettype($data)) {
    case 'string':
      // Do nothing.
      break;

    case 'array':
      $str = '';
      if (isset($data['value'])) {
        $str = $data['value'];
      }
      else {
        foreach ($data as $term) {
          if (isset($term->tid)) {
            $str .= $term->name . ' ';
          }
        }
      }
      if ($str) {
        $data = NULL;
        $data = $str;
      }
      else {
        watchdog('quora', 'Unsupported fieldtype selected as quora_tag_field', array(), WATCHDOG_NOTICE, 'link');
        // Selecting title as quora_tag_field.
        $data = entity_metadata_wrapper('node', $node)->title->value();
      }
      break;

    case 'object':
      $str = '';
      if (isset($data->type)) {
        $str = _quora_preprocess_tag_terms($callby, $data, _quora_fetch_field($data));
      }

      if ($str) {
        $data = NULL;
        $data = $str;
      }
      else {
        watchdog('quora', 'Unsupported fieldtype selected as quora_tag_field', array(), WATCHDOG_NOTICE, 'link');
        // Selecting title as quora_tag_field.
        $data = entity_metadata_wrapper('node', $node)->title->value();
      }
      break;

    default:
      watchdog('quora', 'Unsupported fieldtype selected as quora_tag_field', array(), WATCHDOG_NOTICE, 'link');
      // Selecting title as quora_tag_field.
      $data = entity_metadata_wrapper('node', $node)->title->value();
      break;
  }
  return $data;
}

/**
 * Filters tag terms based on user defined options.
 */
function _quora_filter_tag_terms($callby, &$data, $conf) {
  // We have string in variable data.
  $data = preg_replace('/[^\p{L}\p{N}\ ]/', '', $data);
  $data = str_replace(_quora_preprocess_prepare_exclude_arr($callby, $conf), '', $data);
  $data = _quora_preprocess_sensitivity($callby, $data, $conf);
  $data .= ' ' . _quora_preprocess_prepare_include_arr($callby, $conf);
  $data = preg_replace('/\s+/', ' ', $data);
}


/**
 * Helper function for preprocessor to build exclude list.
 */
function _quora_preprocess_sensitivity($callby, $data, $conf = NULL) {
  $terms = explode(' ', $data);
  $count = count($terms);
  switch (_quora_default($callby, 'search_sensitivity', $conf)) {

    case 1:
      // 3 Words.
      if ($count > 3) {
        $terms = array_slice($terms, 0, 3);
      }
      break;

    case 2:
      // 5 Words.
      if ($count > 5) {
        $terms = array_slice($terms, 0, 5);
      }
      break;

    case 3:
      // 7 Words.
      if ($count > 7) {
        $terms = array_slice($terms, 0, 7);
      }
      break;

    default:
      // Maximum words.
      while ($count >= 10) {
        $count = $count / 2;
      }
      $terms = array_slice($terms, 0, $count);
      break;
  }
  return implode(' ', $terms);
}

/**
 * Helper function for preprocessor to build exclude list.
 */
function _quora_preprocess_prepare_exclude_arr($callby, $conf = NULL) {
  $ex = _quora_default($callby, 'exclude', $conf);
  if ($ex) {
    $ex = preg_replace('/[^\p{L}\p{N}\ \,]/', '', $ex);
    $ex = explode(',', $ex);
  }
  return $ex;
}

/**
 * Helper function for preprocessor to build include string.
 */
function _quora_preprocess_prepare_include_arr($callby, $conf = NULL) {
  $in = _quora_default($callby, 'include', $conf);
  if ($in) {
    $in = preg_replace('/[^\p{L}\p{N}\ ]/', '', $in);
  }
  return $in;
}

/**
 * Search web using google and returns results.
 */
function _quora_search_web($query) {
  $query .= " site:quora.com";
  $query = trim($query);
  $api = variable_get('quora_google_cse_api');
  $cx = variable_get('quora_google_cse_cx');
  if ($api && $cx) {
    // Use api to get results.
    $gs_results = json_decode(@file_get_contents('https://www.googleapis.com/customsearch/v1?key=' . $api . '&cx=' . $cx . '&q=' . urlencode($query)));
    if ($gs_results) {
      foreach ($gs_results->items as $result) {
        $q_results[] = array(
          'title' => $result->title,
          'snippet' => $result->snippet,
          'url' => $result->link,
        );
      }
      return $q_results;
    }
  }
  // Else Use default ajax way to fetch results.
  $gs_results = json_decode(@file_get_contents('http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&q=' . urlencode($query)));
  if ($gs_results) {
    foreach ($gs_results->responseData->results as $result) {
      $q_results[] = array(
        'title' => $result->titleNoFormatting,
        'snippet' => strip_tags($result->content),
        'url' => $result->url,
      );
    }
  }
  else {
    watchdog('quora', 'Unable to fetch results with query @query', array('@query' => urlencode($query)), WATCHDOG_ERROR, 'link');
    return FALSE;
  }
  return $q_results;
}

/**
 * Process results array.
 */
function _quora_process_results($callby, $results, $conf = NULL) {
  $results = array_slice($results, 0, _quora_default($callby, 'no_questions', $conf));
  // Preprocess description snippet to be displayed.
  if (_quora_default($callby, 'description', $conf) == 'disable') {
    foreach ($results as $key => $result) {
      unset($results[$key]['snippet']);
    }
  }
  else {
    $size = _quora_default($callby, 'description_size', $conf);
    if ($size) {
      foreach ($results as $key => $result) {
        $results[$key]['snippet'] = text_summary($result['snippet'], NULL, $size) . '..';
      }
    }
  }
  foreach ($results as $item) {
    $content[] = array(
      '#type' => 'markup',
      '#markup' => theme('quora_que_item', $item),
    );
  }
  return $content;
}

/**
 * Returns a string for a variable name.
 *
 * Used by settings forms to retrieve strings.
 */
function _quora_default($callby, $key, $conf = NULL) {
  switch ($callby) {
    case 'ctools':
      $admin_setting = !empty($conf['quora_' . $key]) ? $conf['quora_' . $key] : FALSE;
      break;

    case 'block':
      $admin_setting = variable_get('quora_' . $key, FALSE);
      break;
  }
  if ($admin_setting) {
    // An admin setting overrides the default string.
    $default = $admin_setting;
  }
  else {
    // No override, return default string.
    switch ($key) {
      case 'no_questions':
        $default = 3;
        break;

      case 'description':
        $default = 'enable';
        break;

      case 'description_size':
        $default = 0;
        break;

      case 'search_sensitivity':
        $default = 0;
        break;

      case 'exclude':
        $default = '';
        break;

      case 'include':
        $default = '';
        break;
    }
  }
  return $default;
}
